<?php

namespace App\Http\Controllers;

use App\Models\Artistes;
use App\Models\Categories;
use Database\Seeders\CategoriesTableSeeder;
use Illuminate\Http\Request;

class CategorieController extends Controller
{
    public function index()
    {
        $categories = Categories::all();
        return view('categorie', compact("categories"));
    }

    public function create()
    {
        $categories = Categories::all();
        return view("createCategorie");
    }

    public function delete(Categories $categorie)
    {
        $categorie->delete();
        return back()->with("successDelete", "Catégorie supprimé avec succès !");
    }

    public function store(Request $request)
    {
        $request->validate([
            "Nom_categorie" => "required",

        ]);
        Categories::create($request->all());

        return back()->with("success", "Catégorie ajouter avec succès !");

    }

    public function update(Request $request, Categories $categorie)
    {
        $request->validate([
            "Nom_categorie" => "required",

        ]);
        $categorie->update([
            "Nom_categorie" => $request->Nom_categorie,

        ]);
        return back()->with("success","Catégorie mis à jour avec succès !");

    }

    public function edit(Categories $categorie)
    {

        return view("EditCategorie", compact('categorie'));
    }




}
