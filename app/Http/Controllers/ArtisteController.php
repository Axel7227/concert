<?php

namespace App\Http\Controllers;

use App\Models\Artistes;
use App\Models\Categories;
use Illuminate\Http\Request;

class ArtisteController extends Controller
{

    public function index()
    {
        $artistes = Artistes::orderBy("Nom","asc")->paginate(10);
        return view("artiste", compact('artistes'));
    }

    public function create()
    {
        $categories = Categories::all();
        return view("createArtiste", compact('categories'));
    }

    public function delete(Artistes $artiste)
        {

$artiste->delete();
return back()->with("successDelete","Artiste supprimé avec succès !");
        }


    public function store(Request $request){
        $request->validate([
            "Nom"=>"required",
            "Prenom"=>"required",
            "Nom_scene"=>"",
            "categorie_id"=>"required"
        ]);
        Artistes::create($request->all());

        return back()->with("success","Artiste ajouté avec succès !");
    }

    public function update(Request $request, Artistes $artiste){
        $request->validate([
            "Nom"=>"required",
            "Prenom"=>"required",
            "Nom_scene"=>"",
            "categorie_id"=>"required"
        ]);
        $artiste->update([
            "Nom"=> $request->Nom,
            "Prenom"=>$request->Prenom,
            "Nom_scene"=>$request->Nom_scene,
            "categorie_id"=>$request->categorie_id
        ]);

        return back()->with("success","Artiste mis à jour avec succès !");
    }

    public function edit(Artistes $artiste)
    {
        $categories = Categories::all();

        return view("EditArtiste", compact('artiste','categories'));
    }

    public function show ($id)
    {
        $artiste = Artistes::find($id);
        return view('artisteShow');
    }
}
