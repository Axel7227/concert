<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Artistes extends Model
{
    use HasFactory;

    protected $fillable = ["Nom","Prenom","Nom_scene","categorie_id"];

    public function categorie(){
        return $this->belongsTo(Categories::class);
    }

    public function maitre()
   {
       return $this->belongsToMany(Artistes::class,'pivot_artistes','artiste_maitre_id');
    }
    public function enfant()
    {
        return $this->belongsToMany(Artistes::class,'pivot_artistes','artiste_enfant_id');
    }
}
