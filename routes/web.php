<?php

use Illuminate\Support\Facades\Route;
use App\http\Controllers\ArtisteController;
use App\Http\Controllers\CategorieController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[\App\Http\Controllers\ConcertController::class, "index"])->name("welcome");

Route::get('/artiste',[ArtisteController::class, "index"])->name("artiste");
Route::get('/artiste/create',[ArtisteController::class, "create"])->name("artiste.create");
Route::post('/artiste/create',[ArtisteController::class, "store"])->name("artiste.ajouter");
Route::delete('/artiste/{artiste}',[ArtisteController::class, "delete"])->name("artiste.supprimer");
Route::put('/artiste/{artiste}',[ArtisteController::class, "update"])->name("artiste.update");
Route::get('/artiste/{artiste}',[ArtisteController::class, "edit"])->name("artiste.edit");
Route::get('/show/{artiste}', [ArtisteController::class, 'show'])->name("artiste.show");



Route::get('/categorie',[CategorieController::class, "index"])->name("categorie");
Route::get('/categorie/create',[CategorieController::class, "create"])->name("categorie.create");
Route::post('/categorie/create',[CategorieController::class, "store"])->name("categorie.ajouter");
Route::delete('/categorie/{categorie}',[CategorieController::class, "delete"])->name("categorie.supprimer");
Route::put('/categorie/{categorie}',[CategorieController::class, "update"])->name("categorie.update");
Route::get('/categorie/{categorie}',[CategorieController::class, "edit"])->name("categorie.edit");
