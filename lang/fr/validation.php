<?php

return[
    'custom'=>[
        'Nom'=>[
            'required' => 'Le champ "Nom" est obligatoire',
        ],
        'prenom'=>[
            'required' => 'Le champ "Prenom" est obligatoire',
        ],
        'nom_scene'=>[
            'required' => 'Le champ "Nom de Scène" est obligatoire',
        ],
        'categorie_id'=>[
            'required' => 'Le champ "Catégorie" est obligatoire',
        ],
    ],
];
