<!doctype html>
<html lang="en">
<style>
    body {
        background-image : url('{{ asset('images/concert.jpg') }}');
        background-size: cover;
        background-position:  center center;
        background-repeat: no-repeat;
        min-height: 100vh;
    }
    table{
        opacity: 1;
    }
    main {
        opacity: 0.8;
    }
</style>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Concert</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/offcanvas/">


    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">


    <link href="{{asset('css/offcanvas.css')}}" rel="stylesheet">
</head>

<body class="bg-light">

<nav class="navbar navbar-expand-md fixed-top navbar-dark bg-dark">
    <a class="navbar-brand" href="#">Concert</a>
    <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="{{route('artiste')}}">Artiste <span class="sr-only"></span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('categorie')}}">Catégorie</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Artiste Associés</a>
            </li>
        </ul>

    </div>
</nav>

<main class="container">
    <div class="my-3 p-3 bg-body rounded shadow-sm">
        <h3 class="border-bottom pb-2 mb-4">Ajouter un artiste :</h3>
        <div class="mt-4">

            @if(session()->has("success"))
                <div class="alert alert-success">
                    <h3>{{session()->get('success')}}</h3>
                </div>
            @endif


            @if ($errors->any())
                <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
                </div>
            @endif
            <form style="width: 65%;" method="post" action="{{route('artiste.ajouter')}}">
                @csrf
                <div class="mb-3">
                    <label for="exampleInputEmail1">Nom :</label>
                    <input type="text" class="form-control" name="Nom">
                </div>
                <div class="mb-3">
                    <label for="exampleInputPassword1">Prénom :</label>
                    <input type="text" class="form-control" name="Prenom">
                </div>
                <div class="mb-3">
                    <label for="exampleInputPassword1">Nom de scène :</label>
                    <input type="text" class="form-control" name="Nom_scene">
                </div>
                <div class="mb-3">
                    <label for="exampleInputPassword1">Catégorie :</label>
                    <select class="form-control" name="categorie_id">
                        <option value=""></option>
                        @foreach($categories as $categorie)
                            <option value="{{$categorie->id}}">{{$categorie->Nom_categorie}}</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Enregistrer</button>
                <a href="{{route('artiste')}}" class="btn btn-danger">Annuler</a>

            </form>
        </div>
    </div>
</main>
<script src="{{asset('js/offcanvas.js')}}" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
