<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Concert</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/offcanvas/">

    <link href="{{asset('css/app.css')}}" rel="stylesheet">

    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">


    <link href="{{asset('css/offcanvas.css')}}" rel="stylesheet">
</head>

<body class="bg-light">

<nav class="navbar navbar-expand-md fixed-top navbar-dark bg-dark">

    <a class="navbar-brand" href="{{route('welcome')}}">Concert</a>
    <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="{{route('artiste')}}">Artiste <span class="sr-only"></span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('categorie')}}">Catégorie</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Artiste Associés</a>
            </li>
        </ul>

    </div>
</nav>

<main class="container">

    <div class="my-3 p-3 bg-body rounded shadow-sm">
        <h3 class="border-bottom pb-2 mb-4"> Détails de l'artiste : </h3>
        <div class="mt-4">
           <h1>{{$artiste->Nom}}</h1>

        </div>
    </div>
</main>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="{{asset('js/offcanvas.js')}}" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
</body>
</html>
<style>
    body {
        background-image : url('{{ asset('images/concert.jpg') }}');
        background-size: cover;
        background-position:  center center;
        background-repeat: no-repeat;
        min-height: 100vh;
    }
    table{
        opacity: 1;
    }
    main {
        opacity: 0.8;
    }
</style>
