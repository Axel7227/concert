<?php

namespace Database\Factories;

use App\Http\Controllers\ArtisteController;
use App\Models\Artistes;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class ArtistesFactory extends Factory
{
    protected $model = Artistes::class;


    public function definition()
    {
        return [
            'Nom'=> $this->faker->lastName,
            'Prenom'=> $this->faker->firstName,
            'Nom_scene'=>$this->faker->lastName,
            'categorie_id'=>rand(1,9)

        ];
    }
}
