<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pivot_artistes', function (Blueprint $table) {
            $table->foreignId('artiste_maitre_id')->constrained('artistes');
            $table->foreignId('artiste_enfant_id')->constrained('artistes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pivot_artistes');
    }
};
