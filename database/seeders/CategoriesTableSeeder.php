<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("categories")->insert([
            ["Nom_categorie"=>"Rock"],
            ["Nom_categorie"=>"Rap"],
            ["Nom_categorie"=>"Pop"],
            ["Nom_categorie"=>"R&B"],
            ["Nom_categorie"=>"Jazz"],
            ["Nom_categorie"=>"Punk"],
            ["Nom_categorie"=>"Metal"],
            ["Nom_categorie"=>"Folk"],
            ["Nom_categorie"=>"Reggae"]
        ]);
    }
}
