<?php

namespace Database\Seeders;

use App\Models\Artistes;
use Database\Factories\ArtistesFactory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Artistes::factory(30)->create();
        //$this->call(CategoriesTableSeeder::class);
    }
}
